function  J = Est_theta(theta,t,eps_inv,E0a,E0b,E1a,E1b,nu1a,nu1b)



dt = t(2) - t(1);






% fun =@(x)cost(F,A,t,R,R0,x(1),x(2),x(3),x(4),x(5))
eps_inv_1 =zeros(size(t)); %initialiser epsilon_inv1
theta_inv =zeros(size(t)); %initialiser theta_inv
E0_inv=zeros(size(t));
E1_inv=zeros(size(t));
nu1_inv=zeros(size(t));

for i=1:1:size(t)
    
    for j=1:3
        E0_inv(i) = (E0a .*theta_inv(i)) + E0b; %Pa
        E1_inv(i) = (E1a .*theta_inv(i)) + E1b; % Pa
        nu1_inv(i) = (nu1a.*theta_inv(i)) + nu1b; % Pa.s
        eps_inv_1(i+1) = abs(eps_inv_1(i) + (dt/nu1_inv(i)) * (theta_inv(i) - (E1_inv(i)*eps_inv_1(i))));
        theta_inv(i) = abs((eps_inv(i) - eps_inv_1(i)) .* E0_inv(i));
    end
end
J = mean((theta - theta_inv).^2);
end
