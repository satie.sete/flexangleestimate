function J = myFun(R,R0,gamma,epsilon)


J = (R0  * (1- epsilon) .* exp(-gamma*epsilon)) - R;

end

