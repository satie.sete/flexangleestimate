%%%%%%% Bending angle estimation

% Abdo-rahmane Anas Laaraibi ~~~  09/01/2023
% FlexAngleEstimate © 2023 by Abdo-Rahmane Anas Laaraibi is licensed under CC BY-NC 4.0. 
% To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/
%% Load data
load('data_eq.mat');

%%

R0 =2000; % Initial resistance (ohms)
R(R>R0)=R0; % saturation at 550 ohms

dt = t(2)-t(1); % time step

% The optimzed parameters using direct model

% Elastic modulus of the spring A0
A0a = 3.5696e+04;
A0b = 6.6994e+05;

% Elastic modulus of the spring A1
A1a = 9.9023e+05;
A1b = 8.9879e+06;

% Viscosity of the damper B1
B1a = 1.0862e+05;
B1b = 3.9687e+05;

% Relaxation parameter gamma
gamma = 7.2e+04;

%% Find epsilon

eps = @(x)myFun(R,R0,gamma,x);
x0 =zeros(size(t));
eps_inv = fsolve(eps,x0);

%% Problem optimization

x0 =[A0a,A0b,A1a,A1b,B1a,B1b]; %initialisation 

a = [];
b = [];
aeq = [];
beq = [];
lb = [3e4;3e4;3e4;3e4;3e3;3e4];
ub = [1e5;1e6;1e6;1e7;1e6;1e6];
nonlcon = [];
%%%%%----------------------%%%%%%%%
% Uncomment these lines to re-optimize the parameters
% Otherwise you use the same parameters go directly to the next section
%%%%%----------------------%%%%%%%%

% fun = @(x)Est_theta(theta,t,eps_inv,x(1),x(2),x(3),x(4),x(5),x(6)); 
% options = optimoptions('ga','Display','iter');
% rng default  % For reproducibility
% nvars = 6;
% X = ga(fun,nvars,a,b,aeq,beq,lb,ub,nonlcon,options);

% Using the new parameters

% A0a = X(1);
% A0b = X(2);
% A1a = X(3);
% A1b = X(4);
% B1a = X(5);
% B1b = X(6);

%% Estimation of the bending angle

eps_inv_1 =zeros(size(t));
theta_bending =zeros(size(t));
A0_inv=zeros(size(t));
A1_inv=zeros(size(t));
B1_inv=zeros(size(t));

for i=1:1:size(t)
    eps_inv_1(i+1) = eps_inv_1(i);
    for j=1:3
        A0_inv(i) = (A0a .*theta_bending(i)) + A0b; %Pa
        A1_inv(i) = (A1a .*theta_bending(i)) + A1b; % Pa
        B1_inv(i) = (B1a.*theta_bending(i)) + B1b; % Pa.s
        eps_inv_1(i+1) = abs(eps_inv_1(i) + (dt/B1_inv(i)) * (theta_bending(i) - (A1_inv(i)*eps_inv_1(i))));
        theta_bending(i) = abs((eps_inv(i) - eps_inv_1(i)) .* A0_inv(i));

    end
end
%% Plot
figure() 
clf
plot(t,theta) 
hold on 
plot(t,theta_bending,'-.') 
% axis([0 150 0 180])
xlabel('Time (s)')
ylabel ('Angle (°)')
legend ('θ' ,'viscoelastic model')

RMSE = sqrt (mean( (theta - theta_bending).^2))