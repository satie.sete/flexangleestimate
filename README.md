## FlexAngleEstimate (Flexible piezoresistive sensors to estimate upper body joint angle using a Nonlinear AutoRegressive eXogenous Neural Model)

In our research, we conducted a comparative analysis of two distinct estimators to determine upper body joint flexion/extension angles. We evaluated an eXogenous Autoregressive Nonlinear Neural Network (NARX) model and a viscoelastic model. Through thorough evaluation, we found that the NARX neural network model outperforms the viscoelastic model, providing higher accuracy with a root mean square error of 4.85°. Therefore, the NARX neural network model is the preferred option for accurately estimating upper body joint flexion/extension angles.

This study is funded by the ANR as part of the PIA EUR DIGISPORT project (ANR-18-EURE-0022).

A. -R. A. Laaraibi, C. Depontailler, G. Jodin, D. Hoareau, N. Bideau and F. Razan, "An innovative wearable sensing system based on flexible piezoresistive sensors to estimate upper body joint angle using a Nonlinear AutoRegressive exogenous Neural Model," in IEEE Sensors Journal, doi: [10.1109/JSEN.2023.3319559](https://ieeexplore.ieee.org/document/10268893). To view a copie of this article, visit : https://hal.science/hal-04227596.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Viscoelastic_model](#viscoelasticmodel)
- [NARX_model](#narxmodel)
- [Contribution](#contributing)
- [License](#license)

## Installation

For this project, the installation of MathWorks Matlab is required.

Visit the official MathWorks website (https://fr.mathworks.com) to obtain the installation files.


To ensure the smooth execution of this project, it is necessary to install the following toolboxes:

- Deep Learning Toolbox: This toolbox provides a comprehensive set of functions and algorithms for designing, training, and deploying deep neural networks. It enables tasks such as image and signal processing, natural language processing, and computer vision. Installing the Deep Learning Toolbox will empower you with powerful tools for implementing cutting-edge deep learning techniques.

- Optimization Toolbox: The Optimization Toolbox offers a wide range of algorithms and tools for solving optimization problems. It includes various optimization techniques, such as linear programming, nonlinear optimization, and constrained optimization. Installing this toolbox will enable you to efficiently optimize and fine-tune the parameters of your models and algorithms.

- Global Optimization Toolbox: The Global Optimization Toolbox focuses on solving complex optimization problems involving multiple variables and potentially non-convex functions. It provides advanced algorithms for global optimization, which explore the entire solution space to find optimal or near-optimal solutions. By installing this toolbox, you will have access to methods that can effectively tackle challenging optimization scenarios in your project.

To proceed with the installation of these toolboxes, please follow the documentation and instructions provided by MathWorks, the official developer of Matlab. The installation process typically involves downloading the toolbox files from their website or authorized sources and following the installation wizard.

## Usage

Please open and execute the Matlab scripts in each folder for both models: [viscoelastic_model.m](https://gitlab.com/satie.sete/flexangleestimate/-/blob/main/Viscoelastic%20model/viscoelastic_model.m) for the viscoelastic model and [NARX_model.m](https://gitlab.com/satie.sete/flexangleestimate/-/blob/main/NARX%20model/NARX_model.m) for the NARX model.


### ViscoelasticModel

The viscoelastic model consists of a main file, two crucial functions, and a dataset.
To delve into further specifics about this model, you can refer to the [OurPreviousWork](https://www.researchgate.net/publication/368728869_Flexible_dynamic_pressure_sensor_for_insole_based_on_inverse_viscoelastic_model), which provides detailed information on our research regarding a flexible dynamic pressure sensor for insoles based on the inverse viscoelastic model.

### NARXmodel

Within the NARX model folder, you will find a main file, the trained neural network, and a dataset. These components collectively contribute to the NARX model, which is designed to enhance prediction capabilities using neural networks.


## Contributing
We appreciate contributions in various forms: coding, bug reports, feature requests, and issue tracking. Your involvement is highly valued!

## Authors 
The study is carried out by the following authors:

- **Abdo-Rahmane Anas Laaraibi** and **Florence Razan** are affiliated with OASIS, IETR UMR CNRS 6164, Université de Rennes 1, 35042 Rennes, France, and also with the Department of Mechatronics and the SATIE Laboratory, UMR CNRS 8029, École Normale Supérieure de Rennes, 35170 Bruz, France.
- **Corentin Depontailler** is affiliated with the Department of Mechatronics, École Normale Supérieure de Rennes, 35170 Bruz, France.
- **Gurvan Jodin** and **Damien Hoareau** are affiliated with the Department of Mechatronics and the SATIE Laboratory, UMR CNRS 8029, École Normale Supérieure de Rennes, 35170 Bruz, France.
- **Nicolas Bideau** is affiliated with the Movement, Sports and Health (M2S) Laboratory, École Normale Supérieure de Rennes, 35170 Bruz, France.
## License

FlexAngleEstimate © 2023 by Abdo-Rahmane Anas Laaraibi is licensed under CC BY-NC 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/