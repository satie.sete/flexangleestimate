%%%%%%% Bending angle estimation

% Created 30-Jan-2023 by Anas Laaraibi
% FlexAngleEstimate © 2023 by Abdo-Rahmane Anas Laaraibi is licensed under CC BY-NC 4.0. 
% To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/

%% Load data

load('data_eq.mat')

% Normalize data
Rn = R/550; %   R - input time series.
Th_n = theta /160; %  theta - feedback time series.

% Convert the normalized data into ”tonndata” format
X = tonndata(Rn,false,false);
T = tonndata(Th_n,false,false);


%% Prepare the Data for Simulation
load net
[x,xi,ai,t] = preparets(net,X,{},T);

%% Test the Network
y = net(x,xi,ai);
performance = perform(net,t,y); 
RMSE = sqrt(performance)*160

% View the Network
% view(net)

% Plots
% Uncomment these lines to enable various plots.
figure, plotregression(t,y)
figure, plotresponse(t,y)
% figure, ploterrcorr(e)
% figure, plotinerrcorr(x,e)
